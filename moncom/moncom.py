"""
Author: Samuel Bryson
Contact: sam.o.bryson@gmail.com

A program for displaying user-defined messages and images on an external monitor.

Messages and images are entered via the controller on:
http://localhost:5000/controller

Messages are displayed on:
http://localhost:5000/
"""

import os
import re
import csv
import xlwt

from config import app, USER_DIR, IMAGES_DIR, TEXT_DIR, LOGS_DIR, SESSION_DIR, USER_OS
from session_handler import Session
from subprocess import check_output
from datetime import datetime
from flask import render_template, request, jsonify, url_for, send_from_directory
from flask_socketio import SocketIO, emit
from flask_basicauth import BasicAuth
from werkzeug.utils import secure_filename


socketio = SocketIO(app)
basic_auth = BasicAuth(app)


@app.route("/")
@basic_auth.required
def display():
    """
    The site on which messages and images are displayed. This should be viewed in full screen, and
    preferably on the Firefox webbrowser, though it may also be compatible with other modern
    browsers. These have not been tested.
    """
    return render_template("display.html")


@app.route("/controller", methods=["GET"])
@basic_auth.required
def controller():
    """
    The page where all message inputs, image inputs, formatting, data exports and setup
    configurations are controlled.
    """

    def get_settings():
        """
        Fetches the current settings (as defined by the list) from static/style.css for
        autofilling inputs on Controller/Setup.
        """
        for key in [
            "bg_col",
            "txt_col",
            "helv_selected_font",
            "tnr_selected_font",
            "arial_selected_font",
            "verd_selected_font",
            "geor_selected_font",
        ]:
            context[key] = get_css_val(key)

    session = Session()
    preloaded_msgs = get_preloaded_msgs()
    image_names = get_image_names()

    context = {
        "preloaded_msgs": preloaded_msgs,
        "notification": "",
        "image_names": image_names,
        "data_dict": session.data_dict,
    }

    context["urls"] = get_url()
    get_settings()

    # if check_for_updates():
    #     context['notification'] = "Updates ready to download!"

    if request.method == "GET":
        return render_template("controller.html", **context)


@socketio.on("new message")
def new_message(data_in):
    """
    Receives a new message sent via the controller, parses the data, sends the
    message to all displays reading localhost:5000/ and logs the data.

    Params:
        data_in (dict): Contains all relevant text message info.
    """
    update_text = data_in["text"]
    time_sent = datetime.now().strftime("%H:%M:%S")
    update_type = data_in["update_type"]
    data_out = {
        "daytime": time_sent,
        "text": update_text,
        "type": update_type,
        "reltime": data_in["reltime"],
    }
    cleaned_text = clean_html(update_text)
    number_words = str(len(cleaned_text.split(" ")))
    emit("new message done", data_out, broadcast=True)

    log(update_type, number_words, update_text)
    new_item = [time_sent, data_in["reltime"], update_type, update_text]

    Session().add(new_item)


@socketio.on("new image")
def new_image(data_in):
    """
    Same as new_message(), but for images.

    Params:
        data_in (dict): Contains all relevant image message info.
    """
    update_text = data_in["text"]
    time_sent = datetime.now().strftime("%H:%M:%S")
    update_type = data_in["update_type"]
    image_location = url_for("image", image_name=update_text)
    data_out = {
        "daytime": time_sent,
        "text": f"[image]: {update_text}",
        "type": update_type,
        "reltime": data_in["reltime"],
        "image": image_location,
    }
    number_words = "_"

    log(update_type, number_words, update_text)
    new_item = [time_sent, data_in["reltime"], update_type, update_text]

    Session().add(new_item)

    emit("new image done", data_out, broadcast=True)


@socketio.on("new formatting")
def new_formatting(data_in):
    """
    Same as new_message(), but for formatting.


    Params:
        data_in (dict): Contains all relevant information on new formatting.
    """
    update_type = data_in["update_type"]
    time_sent = datetime.now().strftime("%H:%M:%S")
    data_out = {
        "daytime": time_sent,
        "type": update_type,
        "reltime": data_in["reltime"],
        "disp_background_colour": data_in["disp_background_colour"],
        "disp_font_colour": data_in["disp_font_colour"],
        "disp_font_size": data_in["disp_font_size"] + "px",
        "disp_font": data_in["disp_font"],
    }
    update_text = (
        f'bg_col: {data_out["disp_background_colour"]},'
        + f'font_col: {data_out["disp_font_colour"]},'
        + f'font_size: {data_out["disp_font_size"]},'
        + f'font: {data_out["disp_font"]}'
    )
    number_words = "_"

    log(update_type, number_words, update_text)
    emit("new formatting done", data_out, broadcast=True)


@socketio.on("clear session")
def clear_session(data_in):
    """
    Simply calls the clear() method of þe session dict.
    """
    if data_in["i_am_certain_button"] == "checked":
        Session().clear()


@app.route("/update", methods=["POST"])
def update():
    """
    The URL to which new image/message lists are uploaded as POST requests via AJAX.

    Files are saved to the relevant user data folder. The options are then sent to the
    their input multiselect in the browser.

    These updates are also logged.

    Returns:
        data (dict): A list of new messages or images, depending on what was uploaded.
                     These are sent to the
    """
    time_sent = datetime.now().strftime("%H:%M:%S")
    data = {"daytime": time_sent}
    if not request.files:
        data["reltime"] = request.form["reltime"]
        update_type = request.form["update_type"]
    else:
        update_type = "new image list"
    if update_type == "new message list":
        msgs_entered = request.form["preloaded_msgs"]
        update_text = f"[{len(msgs_entered)} messages in total]"
        with open(
            os.path.join(TEXT_DIR, f"{get_time_str()}_msgs.txt"), "w+"
        ) as new_msg_file:
            new_msg_file.write(msgs_entered)
        data["preloaded_msgs"] = msgs_entered
        data["new_msgs_success"] = True
        number_words = "_"
    elif update_type == "new image list":
        files_list = [request.files[file] for file in request.files]
        new_image_dir = os.path.join(IMAGES_DIR, get_time_str())
        os.mkdir(new_image_dir)
        secure_filenames = []
        for img_file in files_list:
            filename = secure_filename(img_file.filename)
            secure_filenames.append(filename)
            img_file.save(os.path.join(new_image_dir, filename))
        data["new_imgs_success"] = True
        data["filenames"] = secure_filenames
        number_words = "_"
        update_text = f"[{len(request.files)} images in total]"

    log(update_type, number_words, update_text)
    return jsonify(data)


@app.route("/image/<image_name>")
def image(image_name):
    """
    This URL is called by the browser when an image update is sent.
    """
    image_dir = get_image_names(from_image_view=True)
    return send_from_directory(image_dir, image_name)


@app.route("/download/<filename>")
def export(filename):
    """
    Allows the user to download the .csv or .xls log as saved in upload().
    """
    try:
        assert filename in os.listdir(LOGS_DIR)
    except AssertionError:
        return "404"
    return send_from_directory(LOGS_DIR, filename, as_attachment=True)


@app.route("/upload", methods=["POST"])
def upload():
    """
    Receives message log from controller, parses the messages (and their corresponding information)
    and saves these as the filetype requested by the user (.csv/.xls).

    Returns:
        filename (str): The name of the output file to be automatically appended to /download and
                        subsequently downloaded in the browser.
    """
    request_form = request.form.to_dict()
    daytimes = request_form["daytimes"].split("_;_")
    reltimes = request_form["reltimes"].split("_;_")
    update_types = request_form["types"].split("_;_")
    updates = request_form["messages"].split("_;_")
    current_date = request_form["current_date"]
    session_nr = request_form["session_nr"]
    therapist_id = request_form["therapist_id"]
    filetype = request_form["filetype"]
    time_now = get_time_str()

    if filetype == "csv":
        with open(
            os.path.join(LOGS_DIR, f"{time_now}.csv"), "w"
        ) as message_log_file_csv:
            csv_writer = csv.writer(message_log_file_csv, delimiter=";", quotechar='"')
            csv_writer.writerow(
                [
                    "Date",
                    "Session Number",
                    "Therapist ID",
                    "Daytime",
                    "Time After Start",
                    "Type",
                    "Word Count",
                    "Raw message",
                    "Clean message",
                ]
            )
            for daytime, reltime, update_type, update in zip(
                daytimes[::-1], reltimes[::-1], update_types[::-1], updates[::-1]
            ):
                cleaned_text = clean_html(update)
                word_count = (
                    str(len(cleaned_text.split(" "))) if update_type == "text" else "_"
                )
                csv_writer.writerow(
                    [
                        current_date,
                        session_nr,
                        therapist_id,
                        daytime,
                        reltime,
                        update_type,
                        word_count,
                        update,
                        cleaned_text,
                    ]
                )

    if filetype == "xls":
        wb = xlwt.Workbook()
        sheet = wb.add_sheet("Message log")
        for idx, col_name in enumerate(
            [
                "Date",
                "Session Number",
                "Therapist ID",
                "Daytime",
                "Time After Start",
                "Type",
                "Word Count",
                "Raw message",
                "Clean message",
            ]
        ):
            sheet.write(0, idx, col_name)
        i = 1
        for daytime, reltime, update_type, update in zip(
            daytimes[::-1], reltimes[::-1], update_types[::-1], updates[::-1]
        ):
            cleaned_text = clean_html(update)
            word_count = (
                str(len(cleaned_text.split(" "))) if update_type == "text" else "_"
            )
            sheet.write(i, 0, current_date)
            sheet.write(i, 1, session_nr)
            sheet.write(i, 2, therapist_id)
            sheet.write(i, 3, daytime)
            sheet.write(i, 4, reltime)
            sheet.write(i, 5, update_type)
            sheet.write(i, 6, word_count)
            sheet.write(i, 7, update)
            sheet.write(i, 8, cleaned_text)
            i += 1
        wb.save(os.path.join(LOGS_DIR, f"{time_now}.xls"))

    filename = time_now + "." + filetype
    return filename


def get_css_loc(elemID, offset=0):
    """
    Takes style.css, splits it into lines and finds the index of the elemID requestsed.

    Params:
        elemID (str): The ID of the element whose style is required.
        offset (int): How many lines further the actual style is located

    Returns:
        idx + offset (int): Location of elemID in style.css lines
        style_list[idx+offset] (str): The contents of style.css at that location
    """
    with open("static/style.css", "r") as style_file_r:
        style_list = style_file_r.readlines()
    for idx, style_line in enumerate(style_list):
        if elemID in style_line:
            return idx + offset, style_list[idx + offset]


def get_css_val(key):
    """
    Parses the lines from get_css_loc() to retrieve the actual value of the style.

    Params:
        key (str): Keyword that describes the style in question

    Returns
        [multiple] (str): The value of the style.
    """
    if key == "bg_col":
        _, disp_bg_col_val = get_css_loc("#display-body", offset=1)

        return disp_bg_col_val.split(" ")[-1][:-2]
    elif key == "txt_col":
        _, disp_font_col_val = get_css_loc("#text-here", offset=1)
        return disp_font_col_val.split(" ")[-1][:-2]
    elif key[-4:] == "size":
        _, disp_font_size_val = get_css_loc("#text-here", offset=2)
        font_size = disp_font_size_val.split(" ")[-1][:-2]
        if key.split("_")[0] == "xs" and font_size == "30px":
            return "selected"
        elif key.split("_")[0] == "s" and font_size == "50px":
            return "selected"
        elif key.split("_")[0] == "m" and font_size == "70px":
            return "selected"
        elif key.split("_")[0] == "l" and font_size == "100px":
            return "selected"
        elif key.split("_")[0] == "xl" and font_size == "130px":
            return "selected"
        else:
            return ""
    elif key[-4:] == "font":
        _, disp_font_val = get_css_loc("#display-body", offset=2)
        font = disp_font_val.split(" ")[-1][:-2]
        if key.split("_")[0] == "helv" and font == "Helvetica":
            return "selected"
        elif key.split("_")[0] == "tnr" and font == "Times New Roman":
            return "selected"
        elif key.split("_")[0] == "arial" and font == "Arial":
            return "selected"
        elif key.split("_")[0] == "verd" and font == "Verdana":
            return "selected"
        elif key.split("_")[0] == "geor" and font == "Georgia":
            return "selected"
        else:
            return ""


def get_preloaded_msgs():
    """
    Retrieves the latest file saved in the user's 'text' directory, which contains the last
    messages saved by the user.

    Returns:
        preloaded_msgs (list of str): The individual lines taken from the file
                                      last_updated_filepath.
        (or empty string if file not found)
    """
    try:
        msg_dir = TEXT_DIR
        all_msg_files = os.listdir(msg_dir)
        paths = [os.path.join(msg_dir, filename) for filename in all_msg_files]
        last_updated_filepath = max(paths, key=os.path.getctime)
        with open(last_updated_filepath, "r") as msg_file:
            preloaded_msgs = msg_file.readlines()
        return preloaded_msgs
    except Exception as exc_text:
        error_log(get_time_str(), exc_text)
        return ""


def get_image_names(from_image_view=False):
    """
    Retrieves the contents of the last updated directory in the user's 'images' directory.
    The return value is a list of strings which are used in combination with the URL
    '/image/<image_name>' to retrieve the corresponding image file.

    Returns:
        latest_image_list (list of str): The names of the files contained in the directory.
        (or empty string if no directory found)
    """
    try:
        all_image_dirs = os.listdir(IMAGES_DIR)
        paths = [
            os.path.join(IMAGES_DIR, image_dirname) for image_dirname in all_image_dirs
        ]
        last_updated_dir = max(paths, key=os.path.getctime)
        if from_image_view:
            return last_updated_dir
        latest_image_list = sorted(os.listdir(last_updated_dir))
        return latest_image_list
    except Exception as exc_text:
        error_log(get_time_str(), exc_text)
        return ""


def get_time_str():
    """
    Used as a generator of unique file/directory names.

    Returns:
        datetime.[...] (str): String containing the date and time when the function was called.
    """
    return datetime.now().strftime("%Y-%m-%d_%H%M%S")


def check_dirs_exist():
    """
    Checks if the necessary user directories exist on the system and creates them if not.
    Essential for the first use.
    """
    all_dirs = [
        USER_DIR,
        IMAGES_DIR,
        TEXT_DIR,
        LOGS_DIR,
        SESSION_DIR,
    ]

    for dirname in all_dirs:
        try:
            os.listdir(dirname)
        except FileNotFoundError:
            os.mkdir(dirname)


def clean_html(raw_html):
    """
    Removes html code from a string so that word counts are accurate.

    Params:
        raw_html (str): An entire message, unedited

    Returns:
        clean_text (str): The same string but without anything written inside greater/less
                          than symbols.
    """
    clean_regex = re.compile("<.*?>")
    clean_text = re.sub(clean_regex, "", raw_html)
    return clean_text


def log(update_type, number_words, update_text):
    """
    Logs all info on each update type (new message, new image, new formatting, new message list,
    new image list) such that data can be extracted from here in case of data loss (accidentally
    closing the browser).

    Skips font_size logs as the callback occurs while the slider is moving.
    """
    timestamp = datetime.now().strftime("%Y.%m.%d %H:%M:%S")
    main_log_file_path = os.path.join(LOGS_DIR, "main.log")

    try:
        with open(main_log_file_path, "r") as log_file:
            last_update = log_file.readlines()[-1]
    except FileNotFoundError:
        with open(main_log_file_path, "w+") as _:
            pass
        last_update = "\t"
    except IndexError:
        last_update = "\t"

    last_update_ts = last_update.split("\t")[0].strip()
    if last_update_ts == timestamp and update_type == "new formatting":
        return

    clean_text = clean_html(update_text)
    with open(main_log_file_path, "a+") as log_file:
        log_file.write(
            f"{timestamp} \t{number_words} \t{update_type} "
            + f"\t{update_text} \t{clean_text}\n"
        )


def error_log(timestamp, error_text):
    """
    Logs any errors not previously accounted for.
    """
    with open(os.path.join(USER_DIR, "errors.log"), "a+") as log_file:
        log_file.write(f'{"*" * 50}\n\n{timestamp} \t\n{error_text}\n\n')


def check_for_updates():
    """
    Currently not functional, but this should at some point tell the user automatically that
    updates are ready to install.
    """
    git_status = check_output("git status", shell=True)
    # updates_ready =
    return False


def get_url():
    """
    Prepares a URL to display on controller screen, via which the output can be viewed on another
    computer in the same network as the host.

    Returns:
        url (str): URL to access output on another computer in the local network.
    """
    urls = []
    if USER_OS == "Windows":
        ipconfig = check_output("ipconfig", shell=True).decode("cp1250").split("\n")
        for line in ipconfig:
            if "IPv4" in line and line[line.index(":") + 2 :].strip() != "":
                urls.append("http://" + line[line.index(":") + 2 :].strip() + ":5000")
    elif USER_OS == "Linux":
        ip = check_output("hostname -i | awk '{print $1}'", shell=True)
        if ip.decode("utf-8").strip() != "":
            urls.append("http://" + ip.decode("utf-8").strip() + ":5000")
        print(ip)
    return urls


if __name__ == "__main__":
    check_dirs_exist()
    socketio.run(
        app,
        debug=True,
        host="0.0.0.0",
        allow_unsafe_werkzeug=True,
    )
