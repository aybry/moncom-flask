$("#disp_background_colour").spectrum({
    preferredFormat: "hex",
    showInput: true
});

$("#disp_font_colour").spectrum({
    preferredFormat: "hex",
    showInput: true
});

$("#console_font_colour").spectrum({
    preferredFormat: "hex",
    clickoutFiresChange: true,
    showInput: true
})

function sendToPreview(changedElem) {
    previewElem = $("#preview-box");
    if (changedElem.id == "disp_background_colour") {
        previewElem.css("background-color", changedElem.value);
    } else if (changedElem.id == "disp_font_colour") {
        previewElem.css("color", changedElem.value);
    } else if (changedElem.id == "font-size-range") {
        // font_size = changedElem.options[changedElem.selectedIndex].text;
        // font_size = changedElem.value;
        // previewElem.css("font-size", font_size);
    } else if (changedElem.id == "disp-font-select") {
        font = changedElem.options[changedElem.selectedIndex].text;
        previewElem.css("font-family", font);
    }
}