function sendUpdate(text, updateType) {
    document.getElementById(updateType + "-input").value = text;
    document.getElementById("submit-" + updateType).click();
}


function sendTextIfEnter(event, selectElem) {
    var selectedOptionText = $("#select-preloaded-messages").find(":selected").text();
    selectedOptionText = selectedOptionText.trim()
    if (event.keyCode == 13) {
        event.preventDefault();
        sendUpdate(selectedOptionText, "text");
    }
}


function sendImageIfEnter(event, selectElem) {
    var selectedOptionText = $("#select-image").find(":selected").text();
    selectedOptionText = selectedOptionText.trim()
    if (event.keyCode == 13) {
        event.preventDefault();
        sendUpdate(selectedOptionText, "image");
    };
}


function checkNotification(input) {
    try {
        notifElem = document.getElementById("notif-container");
        if (input != "") {
            notifElem.removeAttribute("hidden");
        }
    } catch(e) {}
}


function getSelectedText(elem) {
    window.getSelection();
}


function triggerCopyToPreview() {
    inputEl = document.getElementById("text-input");
    copyToPreview(inputEl.value);
}


function copyToPreview(text) {
    document.getElementById("preview-text").innerHTML = text;
    // TODO: Set focus back to text box
}


function appendPrepend(tag, predicate="") {

    activeElId = "text-input";
    activeEl = document.getElementById(activeElId);
    if (predicate != "") {predicate = " " + predicate + " "};
    elOpen = "<" + tag + predicate + ">";
    elClose = "</" + tag + ">";
    if (activeElId == "text-input") {
        selectStart = activeEl.selectionStart;
        selectEnd = activeEl.selectionEnd;
        activeEl.focus();
        if (activeEl.selectionStart || activeEl.selectionStart == "0") {
                    activeEl.value = activeEl.value.substring(0, selectStart)
                                     + elOpen
                                     + activeEl.value.substring(selectStart, selectEnd)
                                     + elClose
                                     + activeEl.value.substring(selectEnd, activeEl.value.length)
        } else {
            activeEl.value += elOpen + elClose
            activeEl.focus();
        }
    }

}


// Shortcut handler
$(document).keydown( function(event) {
    if (event.keyCode == 66 && (event.ctrlKey)) {
        event.preventDefault();
        appendPrepend("b")
    } else if (event.keyCode == 85 && (event.ctrlKey)) {
        event.preventDefault();
        appendPrepend("u");
    } else if (event.keyCode == 73 && (event.ctrlKey)) {
        event.preventDefault();
        appendPrepend("i");
    } else {
        return
    }
});


