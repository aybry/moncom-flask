function print(text) {
    console.log(text);
}


function sidenavController(selectedElem) {
    var sidenavDiv = document.getElementById("sidenav");
    var sidenavAs = sidenavDiv.getElementsByTagName('a');
    for (i = 0; i < sidenavAs.length; i++) {
        sidenavAs[i].removeAttribute("class");
    }
    selectedElem.setAttribute("class", "active");
}


function showContainer(contID) {
    var subconts = document.getElementsByClassName("main-subcontainer");
    for (i = 0; i < subconts.length; i++) {
        subconts[i].setAttribute("hidden", "");
    }
    document.getElementById(contID).removeAttribute("hidden");
}


function pad(value) {
    var string = value + "";
    if (string.length == 2) {
        return string;
    } else {
        return "0" + string;
    }
}


function countUp() {
    ++ secondsTotal;
    var hours = parseInt(secondsTotal / 3600);
    hoursLabel.innerHTML = hours;
    var minutes = parseInt(secondsTotal / 60) - hours * 60;
    minutesLabel.innerHTML = pad(minutes);
    var seconds = secondsTotal % 60;
    secondsLabel.innerHTML = pad(seconds);
    if (hours > 0) {
        hoursLabel.removeAttribute('hidden');
        document.getElementById('hidden-colon').removeAttribute('hidden');
    }
}


function resetTimer(restart=false) {
    clearInterval(timerInterval);
    secondsTotal = 0;
    hoursLabel.innerHTML = "00";
    minutesLabel.innerHTML = "00";
    secondsLabel.innerHTML = "00";
    if (restart) {
        startTiming();
    }
}

function activateTimer() {
    startButton.setAttribute('hidden', '');
    resetButton.setAttribute('hidden', '');
    stopButton.removeAttribute('hidden');
    resetRestartButton.removeAttribute('hidden');
    startTiming();
}


function startTiming() {
    timerInterval = setInterval(countUp, 1000);
}


function deactivateTimer() {
    stopButton.setAttribute('hidden', '');
    resetRestartButton.setAttribute('hidden', '');
    startButton.removeAttribute('hidden');
    resetButton.removeAttribute('hidden');
    stopTiming();
}


function stopTiming() {
    clearInterval(timerInterval);
}


function updatePreloadedMsgs() {
    allMessages = $("#preloaded-messages").val();
    msgSelectElem = document.getElementById("select-preloaded-messages");
    while (msgSelectElem.firstChild) {
        msgSelectElem.removeChild(msgSelectElem.firstChild);
    }
    messageArray = allMessages.split('\n');
    for (i = 0; i < messageArray.length; i++) {
        msgSelectOption = document.createElement("option");
        msgSelectOption.innerText = messageArray[i];
        msgSelectOption.setAttribute("ondblclick", "sendUpdate(this.innerText, 'text');");
        msgSelectElem.appendChild(msgSelectOption);
    }
}


function updatePreloadedImgs(filenames) {
    selectImageElem = $("#select-image")[0];
    selectImageElem.innerHTML = "";
    for (i = 0; i < filenames.length; i++) {
        newOption = document.createElement("option");
        newOption.setAttribute("ondblclick", "sendUpdate(this.innerText, 'image');")
        newOption.innerHTML = filenames[i];
        selectImageElem.appendChild(newOption);
    }
}



function getRelTime() {
    relTimeHours = hoursLabel.innerHTML;
    relTimeMins = minutesLabel.innerHTML;
    relTimeSecs = secondsLabel.innerHTML;
    relTime = relTimeHours + ":" + relTimeMins + ":" + relTimeSecs;
    return relTime
};


function setDate() {
    var today = new Date();
    var dd = ("0" + (today.getDate())).slice(-2);
    var mm = ("0" + (today.getMonth() +　1)).slice(-2);
    var yyyy = today.getFullYear();
    today = yyyy + '-' + mm + '-' + dd ;
    $("#date-input").attr("value", today);
}


function newLine(daytime_in, reltime_in, type_in, text_in) {
    document.getElementById("text-input").value = "";
    var tableBody = document.getElementById("table-body");
    var trElem = document.createElement("tr");
    var tdDayTime = document.createElement("td");
    tdDayTime.setAttribute("hidden", "")
    var tdRelTime = document.createElement("td");
    var tdType = document.createElement("td");
    var tdText = document.createElement("td");
    tdDayTime.innerText = daytime_in;
    tdRelTime.innerText = reltime_in;
    tdType.innerText = type_in;
    tdText.innerText = text_in;
    trElem.appendChild(tdDayTime);
    trElem.appendChild(tdRelTime);
    trElem.appendChild(tdType);
    trElem.appendChild(tdText);
    tableBody.prepend(trElem);
}


function fillTable(sessionData) {
    for (i = 0; i < sessionData.messages.reltimes.length; i++) {
        daytime_in = sessionData.messages.daytimes[i];
        reltime_in = sessionData.messages.reltimes[i];
        type_in = sessionData.messages.types[i];
        text_in = sessionData.messages.updates[i];
        newLine(daytime_in, reltime_in, type_in, text_in);
    }
}



$(document).ready(function() {

    var hoursLabel = document.getElementById("hours")
    var minutesLabel = document.getElementById("minutes")
    var secondsLabel = document.getElementById("seconds")

    socket = io.connect('http://' + document.domain + ':' + location.port);


    $("#update-form").on("submit", function(event) {

        event.preventDefault();
        relTime = getRelTime();

        socket.emit('new message', {
            update_type: 'text',
            text: $("#text-input").val(),
            reltime: relTime
        });
    });

    $("#update-img-form").on("submit", function(event) {

        event.preventDefault();
        relTime = getRelTime();

        socket.emit('new image', {
            update_type: 'image',
            text: $("#image-input").val(),
            reltime: relTime
        });
    });

    $("#display-format-input-form").on("submit", function(event) {
        event.preventDefault();
        relTime = getRelTime();

        socket.emit('new formatting', {
            update_type: 'new formatting',
            disp_background_colour: $("#disp_background_colour").val(),
            disp_font_colour: $("#disp_font_colour").val(),
            disp_font_size: $("#font-size-range").val(),
            disp_font: $("#disp-font-select").val(),
            reltime: relTime
        })
    });

    $("#clear-session-input-form").on("submit", function(event) {
        event.preventDefault();
        tableBodyElem = document.getElementById("table-body");
        while (tableBodyElem.firstChild) {
            tableBodyElem.removeChild(tableBodyElem.firstChild);
        }

        socket.emit('clear session', {
            'i_am_certain_button': 'checked',
        })
    });

    socket.on('new message done', function(data) {
        newLine(data.daytime, data.reltime, "text", data.text)
    })

    socket.on('new image done', function(data) {
        newLine(data.daytime, data.reltime, "image", data.text.slice(9))
    });


    // Exporting message log
    $("#export-form").on("submit", function(event) {

        event.preventDefault();

        var tableBodyElem = document.getElementById("table-body");
        trElems = tableBodyElem.getElementsByTagName("tr");
        daytimes = [];
        reltimes = [];
        types = [];
        messages = [];
        for (i = 0; i < trElems.length; i++) {
            daytimes.push(trElems[i].childNodes[0].innerText);
            reltimes.push(trElems[i].childNodes[1].innerText);
            types.push(trElems[i].childNodes[2].innerText);
            messages.push(trElems[i].childNodes[3].innerText);
        }
        var dayTimesElem = document.getElementById("daytimes-textbox");
        var relTimesElem = document.getElementById("reltimes-textbox");
        var typesElem = document.getElementById("types-textbox");
        var messagesElem = document.getElementById("messages-textbox");
        dayTimesElem.value = daytimes.join("_;_");
        relTimesElem.value = reltimes.join("_;_");
        typesElem.value = types.join("_;_");
        messagesElem.value = messages.join("_;_");
        var currentDate = $("#date-input").val();
        var sessionNr = $("#session-number-input").val();
        var therapistID = $("#therapist-id-input").val();

        $.ajax({
            type: "POST",
            url: "/upload",
            data: {
                daytimes: $("#daytimes-textbox").val(),
                reltimes: $("#reltimes-textbox").val(),
                types: $("#types-textbox").val(),
                messages: $("#messages-textbox").val(),
                filetype: $("#filetype-textbox").val(),
                current_date: currentDate,
                session_nr: sessionNr,
                therapist_id: therapistID
            }
        }).done( function(data) {
            downloadAElem = document.getElementById("download-a");
            downloadAElem.setAttribute("href", "download/" + data);
            document.getElementById("download-button").click();
        });
    });


    $("#new-messages-input-form").on("submit", function(event) {

        event.preventDefault();
        relTime = getRelTime();

        $.ajax({
            data: {
                update_type: 'new message list',
                preloaded_msgs: $("#preloaded-messages").val(),
                reltime: relTime
            },
            type: "POST",
            url: "/update"
        }).done(function(data) {
            if (data.new_msgs_success) {
                $("#new-messages-success").text("Success!");
                $("#new-messages-success").fadeOut(1200);
                updatePreloadedMsgs();
            }
        });
    });

    $("#new-images-input-form").on("submit", function (event) {

        event.preventDefault();
        relTime = getRelTime();
        var files = $("#image-input-elem").get(0).files;
        var formData = new FormData();

        for (var i = 0; i < files.length; i++) {
            formData.append("image" + i, files[i]);
        }

        $.ajax({
            type: 'POST',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            url: "/update"
        }).done(function(data) {
            if (data.new_imgs_success) {
                $("#new-images-success").text("Success!");
                $("#new-images-success").fadeOut(1200);
                updatePreloadedImgs(data.filenames);
            }
        });

    });

    $("#console_font_colour").on("change", function(event) {
        // appendPrepend("text", "style=\"color:" + this.value + "\"");
        // triggerCopyToPreview();
        print(this.value)
        $("#custom-colour-button").attr("disabled", false);
        $("#custom-colour-button").attr("style", "color: " + this.value + "; border-color: " + this.value + ";");
        $("#custom-colour-button").val(this.value)
    })

    $("#disp_background_colour").on("change", function(event) {
        bg_col_elem = document.getElementById("disp_background_colour");
        sendToPreview(bg_col_elem);
        $("#display-format-input-form").submit()
    })

    $("#disp_font_colour").on("change", function(event) {
        font_col_elem = document.getElementById("disp_font_colour");
        sendToPreview(font_col_elem);
        $("#display-format-input-form").submit()
    })

    $("#font-size-range").on("change", function(event) {
        $("#font-size-num").val($("#font-size-range").val());
        $("#display-format-input-form").submit();
    })

    $("#font-size-range").on("input", function(event) {
        $("#font-size-num").val($("#font-size-range").val());
        $("#display-format-input-form").submit();
    })

    $("#font-size-num").on("change", function(event) {
        $("#font-size-range").val($("#font-size-num").val());
        $("#display-format-input-form").submit();
    })

    $("#disp-font-select").on("change", function(event) {
        $("#display-format-input-form").submit();
    })

    updatePreloadedMsgs();
    setDate();

});





var timerInterval;
var secondsTotal = 0;
var startButton = document.getElementById('start-button')
var resetButton = document.getElementById('reset-button')
var resetRestartButton = document.getElementById('reset-restart-button')
var stopButton = document.getElementById('stop-button')
var hoursLabel = document.getElementById('hours');
var minutesLabel = document.getElementById('minutes');
var secondsLabel = document.getElementById('seconds');


