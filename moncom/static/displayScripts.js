$(document).css('cursor', 'none');

function print(object) {
    // Lazy man's console.log. I hate console.log.

    console.log(object);

}


$(document).ready(function() {

    textElem = document.getElementById('text-here');
    imgElem = document.getElementById('image-here');
    socket = io.connect('http://' + document.domain + ':' + location.port);

    socket.on('new message done', function(data) {
        imgElem.removeAttribute('src');
        textElem.innerHTML = data.text;
    });

    socket.on('new image done', function(data) {
        imgElem.setAttribute('src', data.image);
        textElem.innerText = '';
    })

    socket.on('new formatting done', function(data) {
        $("#display-body").css("background-color", data.disp_background_colour);
        $("#display-body").css("font-family", data.disp_font);
        $("#text-here").css("color", data.disp_font_colour);
        $("#text-here").css("font-size", data.disp_font_size);
    })

});

var socket;