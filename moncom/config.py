import os
import platform

from flask import Flask


USER_OS = platform.system()

app = Flask(__name__)

if USER_OS == "Windows":
    USER_DIR = os.path.join(os.getenv("appdata"), "moncom")
elif USER_OS == "Linux":
    USER_DIR = os.path.join(os.path.expanduser("~"), ".moncom")
else:
    print("Platform not recognised. Get in touch with dev@ay-bryson.com")

IMAGES_DIR = os.path.join(USER_DIR, "images")
TEXT_DIR = os.path.join(USER_DIR, "text")
LOGS_DIR = os.path.join(USER_DIR, "message_logs")
SESSION_DIR = os.path.join(USER_DIR, "session_data")

app.config["BASIC_AUTH_USERNAME"] = "videolabor"
app.config["BASIC_AUTH_PASSWORD"] = "Kei te pai"
