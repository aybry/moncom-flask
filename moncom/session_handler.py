import os
import json

from config import SESSION_DIR


class Session(object):
    def __init__(self):
        self.fp = os.path.join(SESSION_DIR, "session.json")

        if not os.path.isfile(self.fp) or os.path.getsize(self.fp) < 150:
            self.clear()
        self.get()

    def get(self):
        with open(self.fp, "r") as session_f_r:
            session_data = json.load(session_f_r)
        self.data_dict = session_data

    def add(self, new_item):
        if not os.path.isfile(self.fp):
            self.clear()

        with open(self.fp, "r") as session_f_r:
            data = json.load(session_f_r)

        with open(self.fp, "w+") as session_f_w:
            data["messages"]["daytimes"].append(new_item[0])
            data["messages"]["reltimes"].append(new_item[1])
            data["messages"]["types"].append(new_item[2])
            data["messages"]["updates"].append(new_item[3])
            json.dump(data, session_f_w, indent=4)

    def clear(self):
        blank_session = {
            "messages": {
                "daytimes": [],
                "reltimes": [],
                "types": [],
                "updates": [],
            }
        }
        with open(self.fp, "w+") as session_f:
            json.dump(blank_session, session_f, indent=4)
